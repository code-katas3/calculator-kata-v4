﻿using System;
using System.Linq;
using System.Collections.Generic;
using static System.Exception;
using System.Collections;

namespace Calculator
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {

            string[] delimiters = ExtractDelimiters(ref numbers);
            var arList = new ArrayList(delimiters);
            if (string.IsNullOrEmpty(numbers))
                return 0;

            int sum = 0;

            var negativeNumbers = new List<int>();
            var parsedNumbers = numbers.Split(delimiters.ToArray(), StringSplitOptions.None).Select(int.Parse);
            foreach (int number in parsedNumbers.Where(n => n < 1001))
            {
                if (number < 0)
                    negativeNumbers.Add(number);
                sum += number;

            }
            if (negativeNumbers.Any())
            {
                throw new Exception("Negatives not allowed : " + string.Join(" , ", negativeNumbers));
            }
            return sum;
        }

        private static string[] ExtractDelimiters(ref string numbers)
        {

            if (!numbers.StartsWith("//"))
                return new[] { ",", "\n" };

            string[] delimiter;

            if (numbers.StartsWith("//["))
            {

                int delimiterEndIndex = numbers.LastIndexOf("]\n");
                delimiter = numbers.Substring(3, delimiterEndIndex - 3).Split(new[] { "][" }, StringSplitOptions.None);
                numbers = numbers.Substring(delimiterEndIndex + 2);

            }
            else if (numbers.StartsWith("//"))
            {
                int delimiterEndIndex = numbers.IndexOf("\n");
                delimiter = numbers.Substring(2, delimiterEndIndex - 2).Split(new[] { "][" }, StringSplitOptions.None);
                numbers = numbers.Substring(delimiterEndIndex + 1);
            }

            else
            {
                delimiter = new[] { numbers[2].ToString() };
                numbers = numbers.Substring(4);
            }
            return delimiter;
        }

    }
}
